---
layout: default
title: Home
nav_order: 1
description: "Material and links to material for the networking lectures at the IT technology education at UCL Odense Denmark. Networking is primarily about the TCP/IP stack and Ethernet communication."
permalink: /
---

# Networking

Material and links to material for the networking lectures at the IT technology education at UCL Odense Denmark. Networking is primarily about the TCP/IP stack and Ethernet communication.