---
layout: page
title: Material 1
parent: Material
description: Material 1 page
nav_order: 2
---

# Material 1

Write an article for a networking subject, this will go to a seperate page in the Material menu

Change title and description in the frontmatter on the top of the page.

Parent has to point to the top item in Material (index.md title)