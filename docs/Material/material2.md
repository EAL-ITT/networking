---
layout: page
title: Material 2
parent: Material
description: Material 2 page
nav_order: 3
---

# Material 2

Write an article for a networking subject, this will go to a seperate page in the Material menu

Change title and description in the frontmatter on the top of the page.

Parent has to point to the top item in Material (index.md title)