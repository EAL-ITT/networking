---
layout: page
title: Links
nav_order: 3
has_children: false
permalink: /links/
---

# Links

This page contain links 

A link can be written like 
```html
<a href="https://eal-itt.gitlab.io/" target="_blank">https://eal-itt.gitlab.io/</a>
```

or 

```markdown
[https://eal-itt.gitlab.io/](https://eal-itt.gitlab.io/)
```

See more in the cheatsheet:  
<a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet" target="_blank">https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet</a>

